package com.example.assignment.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.assignment.Dialog.AddTodoTypeBottomSheet;
import com.example.assignment.Dialog.UpdateTodoTypeBottomSheet;
import com.example.assignment.Modal.TodoType;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;

public class TodoTypeAdapter extends RecyclerView.Adapter<TodoTypeAdapter.TodoTypeViewHolder> implements Filterable {
    Context mContext;
    List<TodoType> mData ;
    List<TodoType> mDataFiltered ;
    private RetrofitSevice sevice;
    public TodoTypeAdapter(Context mContext, List<TodoType> mData) {
        this.mContext = mContext;
        this.mData = mData;
        this.mDataFiltered = mData;
    }

    public class TodoTypeViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title;
        CircleImageView imgTodoType;
        ImageView imgDelete,imgEdit;
        LinearLayout container;

        public TodoTypeViewHolder(@NonNull View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            tv_title = itemView.findViewById(R.id.tv_title);
            imgTodoType = itemView.findViewById(R.id.imgTodoType);
            imgDelete = itemView.findViewById(R.id.imgDelete);
            imgEdit = itemView.findViewById(R.id.imgEdit);

        }

    }
    @NonNull
    @Override
    public TodoTypeAdapter.TodoTypeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.item_todo_type,viewGroup,false );
        TodoTypeViewHolder todoTypeViewHolder =new TodoTypeViewHolder(view);
        return todoTypeViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TodoTypeAdapter.TodoTypeViewHolder todoTypeViewHolder, int position) {
        todoTypeViewHolder.tv_title.setText(mDataFiltered.get(position).getTypeName());
        if(mDataFiltered.get(position).getImage().equals(BASE_URL+"image/")) {
            todoTypeViewHolder.imgTodoType.setImageResource( R.drawable.product );
        }else{
            Picasso.get().load(mDataFiltered.get(position).getImage()).into(todoTypeViewHolder.imgTodoType);

        }
        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);

        todoTypeViewHolder.imgDelete.setOnClickListener((view)->{
            Map<String, String> fields= new HashMap<>();
            fields.put("id",String.valueOf(mDataFiltered.get(position).getId()));
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
            builder.setTitle("Delete");
            builder.setMessage("Do you delete "+mDataFiltered.get( position ).getTypeName()+ "?");
            builder.setCancelable(true);
            builder.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
            sevice.deleteTodoType(fields).enqueue(getDeleteTodoTypeCallback);
            mDataFiltered.remove(position);
                            notifyDataSetChanged();                        }
                    });

            builder.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        });

        todoTypeViewHolder.imgEdit.setOnClickListener((view)->{
            Bundle args = new Bundle();
            args.putString("ID", mDataFiltered.get(position).getId()+"");
            args.putString("typeName", mDataFiltered.get(position).getTypeName()+"");
            args.putString("img", mDataFiltered.get(position).getImage());
            UpdateTodoTypeBottomSheet todoTypeBottomSheet= new UpdateTodoTypeBottomSheet();
            todoTypeBottomSheet.setArguments( args );
            todoTypeBottomSheet.show(((AppCompatActivity) mContext).getSupportFragmentManager(),todoTypeBottomSheet.getTag());
        });

    }

    Callback<TodoType> getDeleteTodoTypeCallback = new Callback<TodoType>(){

        @Override
        public void onResponse(Call<TodoType> call, Response<TodoType> response) {
            if(response.isSuccessful()){

            }else{
                Log.e("b", "onResponse: "+response.message());
            }
        }

        @Override
        public void onFailure(Call<TodoType> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };

    @Override
    public int getItemCount() {
        return mDataFiltered.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String Key = constraint.toString();
                if (Key.isEmpty()) {

                    mDataFiltered = mData ;

                }
                else {
                    List<TodoType> lstFiltered = new ArrayList<>();
                    for (TodoType row : mData) {

                        if (row.getTypeName().toLowerCase().contains(Key.toLowerCase())){
                            lstFiltered.add(row);
                        }

                    }

                    mDataFiltered = lstFiltered;

                }


                FilterResults filterResults = new FilterResults();
                filterResults.values= mDataFiltered;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {


                mDataFiltered = (List<TodoType>) results.values;
                notifyDataSetChanged();

            }
        };

    }



}
