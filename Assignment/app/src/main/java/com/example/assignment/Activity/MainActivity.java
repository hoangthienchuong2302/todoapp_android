package com.example.assignment.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.example.assignment.Fragment.TodoTypeFragment;
import com.example.assignment.Fragment.ProfileFragment;
import com.example.assignment.Fragment.TodoListFragments;
import com.example.assignment.Modal.User;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.jetbrains.annotations.NotNull;



import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "PushNotification";
    private static final String CHANEL_ID = "abc";
    public static MeowBottomNavigation bottommeo;
    public static  User user;
    public static String idFinal;
    public TodoTypeFragment todoTypeFragment;
    TextView titletoolbar;
    private RetrofitSevice sevice;
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottommeo = (MeowBottomNavigation) findViewById(R.id.bottommeo);
        bottommeo.add(new MeowBottomNavigation.Model(1, R.drawable.todo_type));
        bottommeo.add(new MeowBottomNavigation.Model(2, R.drawable.todo_list));
        bottommeo.add(new MeowBottomNavigation.Model(3, R.drawable.iconuser));
        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);

        createNotifcationChanel();
        getToken();

        titletoolbar = findViewById(R.id.toolbar_title);
        getSupportFragmentManager().beginTransaction().replace(R.id.fr_l, new TodoTypeFragment()).commit();

        bottommeo.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {

            }
        });
        bottommeo.setOnShowListener(new MeowBottomNavigation.ShowListener() {
            @Override
            public void onShowItem(MeowBottomNavigation.Model item) {
                Fragment fragment = null;
                switch (item.getId()) {
                    case 1:
                        fragment = new TodoTypeFragment();
                        titletoolbar.setText("Todo Type");
                        break;
                    case 2:
                        fragment = new TodoListFragments();
                        titletoolbar.setText("Todo List");
                        break;
                    case 3:
                        fragment = new ProfileFragment();
                        titletoolbar.setText("Profile");
                        break;
                }

                getSupportFragmentManager().beginTransaction().replace(R.id.fr_l, fragment).commit();
            }
        });
    }

    private void getToken(){
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<String> task) {
                if(!task.isSuccessful()){
                    Log.e(TAG, "onComplete: "+"Failed to getToken");
                }
                String token = task.getResult();
                Log.e(TAG, "onComplete: "+token);
            }
        });
    }

    private void createNotifcationChanel(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = "firebaseNotifyChanel";
            String description = "Receve Firebase Notification";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(CHANEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}