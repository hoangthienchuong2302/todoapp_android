package com.example.assignment.Modal;

public class Todo {


    private String title, desciption, startDate,endDate, isDone;
    private int idUser,id,idTodoType;

    public String getIsDone() {
        return isDone;
    }

    public void setIsDone(String isDone) {
        this.isDone = isDone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTodoType() {
        return idTodoType;
    }

    public void setIdTodoType(int idTodoType) {
        this.idTodoType = idTodoType;
    }

    public Todo(String title, String desciption, String startDate, String endDate, int idUser, int idTodoType) {
        this.title = title;
        this.desciption = desciption;
        this.startDate = startDate;
        this.endDate = endDate;
        this.idUser = idUser;
        this.idTodoType = idTodoType;
    }

    public Todo(String title, String desciption, String startDate, String endDate, String isDone, int idUser, int idTodoType) {
        this.title = title;
        this.desciption = desciption;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isDone = isDone;
        this.idUser = idUser;
        this.idTodoType = idTodoType;
    }
}
