package com.example.assignment.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.assignment.Modal.User;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;

public class SplashActivity extends AppCompatActivity {

    private static int DELAY_TIME = 4000;
    public User user;
    Animation topAnim, bottomAnim;
    ImageView imageView;
    TextView app_name;
    public static String idPublic;
    private RetrofitSevice sevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);

        topAnim = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this,R.anim.bottom_animation);

        imageView = findViewById(R.id.imageView2);
        app_name = findViewById(R.id.app_name);

        imageView.setAnimation(topAnim);
        app_name.setAnimation(bottomAnim);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedPreferences = getSharedPreferences("checkLogin",MODE_PRIVATE);
                int id = sharedPreferences.getInt("id",0);
                Map<String, String> idUser= new HashMap<>();
                idUser.put("id", String.valueOf(id));
                sevice.loginById(idUser).enqueue(new Callback<User>(){

                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        if(response.body()!=null){
                            user = response.body();
                            Intent i = new Intent(SplashActivity.this,MainActivity.class);
                            i.putExtra("id",user.getId()) ;
                            idPublic= String.valueOf(user.getId());

                            overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                            startActivity(i);
                            finish();

                        }else{
                            Intent i = new Intent(SplashActivity.this,LoginActivity.class);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                            startActivity(i);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Log.e("c", "onFailure: " + t.getMessage());
                    }
                });


            }
        },DELAY_TIME);
    }
}