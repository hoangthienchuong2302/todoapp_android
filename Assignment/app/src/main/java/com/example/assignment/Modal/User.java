package com.example.assignment.Modal;

public class User {
    private String email,password,confirm_password,fullName,phoneNumber,image, checkLogin;
    private int id;
    public String getEmail() {
        return email;
    }

    public String getCheckLogin() {
        return checkLogin;
    }

    public void setCheckLogin(String checkLogin) {
        this.checkLogin = checkLogin;
    }

    public String getImage() {
        return image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public User(String email, String password, String confirm_password, String fullName, String phoneNumber, String image, int id) {
        this.email = email;
        this.password = password;
        this.confirm_password = confirm_password;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.id = id;
    }

    public User(String email, String password, String confirm_password, String fullName, String phoneNumber, String image, String checkLogin) {
        this.email = email;
        this.password = password;
        this.confirm_password = confirm_password;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.checkLogin = checkLogin;
    }
}
