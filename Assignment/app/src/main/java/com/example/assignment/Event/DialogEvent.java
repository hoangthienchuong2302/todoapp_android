package com.example.assignment.Event;

import com.example.assignment.Modal.Todo;

import java.util.Map;

public class DialogEvent {
    public interface CompleteListener{
        public void complete(String name);
    }
    public interface getToDo {
        public void getToDo(Todo todo);
    }
    public interface AddTodo{
        public void onComplete(Map<String, String> body);
    }
}
