package com.example.assignment.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.transition.CircularPropagation;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.assignment.Modal.Todo;
import com.example.assignment.Modal.User;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;

public class RegisterActivity extends AppCompatActivity {
    public EditText edtName, edtEmail,edtMobile,edtPassword,edtConfirmPassword;
    public CircularProgressButton cirRegisterButton;
    public ImageView imgRegister;
    int touch=0;
    String img_URl="";
    private RetrofitSevice sevice;
    public  List<User> ListUser = new ArrayList<>();
    int RequestCodeImage = 123;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    String realPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        verifyStoragePermissions(RegisterActivity.this);
        edtName= findViewById(R.id.edtName);
        edtEmail= findViewById(R.id.edtEmail);
        edtMobile= findViewById(R.id.edtMobile);
        edtPassword= findViewById(R.id.edtPassword);
        edtConfirmPassword= findViewById(R.id.edtConfirmPassword);
        imgRegister= findViewById(R.id.imgRegister);
        cirRegisterButton= findViewById(R.id.cirRegisterButton);


        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);
        changeStatusBarColor();

        imgRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,RequestCodeImage);
                touch++;
            }
        });

        cirRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateName() && validatePassword() && validatePhoneNo() && validateEmail()){
                    Map<String, String> fields= new HashMap<>();
                    fields.put("fullName", edtName.getText().toString());
                    fields.put("email", edtEmail.getText().toString());
                    fields.put("phoneNumber", edtMobile.getText().toString());
                    fields.put("password", edtPassword.getText().toString());
                    fields.put("confirm_password", edtConfirmPassword.getText().toString());
                    if(touch==0){
                        fields.put("image", BASE_URL + "image/");
                    }else {
                        fields.put("image", BASE_URL + "image/" + img_URl);
                    }
                    sevice.register(fields).enqueue(getRegisterCallback);
            }
            }
        });
    }
    Callback<String> getImageCallback = new Callback<String>(){

        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            if(response.isSuccessful()){
                img_URl=response.body();

            }else{
                Log.e("b", "onResponse: "+response.message());
            }
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            Log.e("image", "onFailure: " + t.getMessage());
        }
    };

    Callback<User> getRegisterCallback = new Callback<User>(){

        @Override
        public void onResponse(Call<User> call, Response<User> response) {
            if(response.isSuccessful()){
                Toast.makeText(RegisterActivity.this, "Register success!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                overridePendingTransition(R.anim.slide_in_left,android.R.anim.slide_out_right);
            }else{
                Log.e("b", "onResponse: "+response.message());
            }
        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            window.setStatusBarColor(getResources().getColor(R.color.register_bk_color));
        }
    }

    public void onLoginClick(View view){
        startActivity(new Intent(this,LoginActivity.class));
        overridePendingTransition(R.anim.slide_in_left,android.R.anim.slide_out_right);

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RequestCodeImage && resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();
            realPath = getRealPathFromUri(uri);
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imgRegister.setImageBitmap(bitmap);
                Log.e("TAG", "onActivityResult: "+uri);

                File file = new File(realPath);
                String filePath = file.getAbsolutePath();
                String[] arrayFileName = filePath.split("\\.");

                filePath = arrayFileName[0] + System.currentTimeMillis() + "." + arrayFileName[1];
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("uploaded_file",filePath,requestBody);
                sevice.UploadImage(part).enqueue(getImageCallback);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public String getRealPathFromUri(Uri contentUri){
        String path = null;
        String[] project = { MediaStore.MediaColumns.DATA };
        Cursor cursor = getContentResolver().query(contentUri, project, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(columnIndex);
        }
        cursor.close();
        return path;
    }
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
    private Boolean validateName() {
        String val = edtName.getText().toString();

        if (val.isEmpty()) {
            edtName.setError("Field cannot be empty");
            return false;
        }
        else {
            edtName.setError(null);
            return true;
        }
    }
    private Boolean validateEmail() {
        String val = edtEmail.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (val.isEmpty()) {
            edtEmail.setError("Field cannot be empty");
            return false;
        } else if (!val.matches(emailPattern)) {
            edtEmail.setError("Invalid email address");
            return false;
        } else {
            edtEmail.setError(null);
            return true;
        }
    }
    private Boolean validatePhoneNo() {
        String val = edtMobile.getText().toString();

        if (val.isEmpty()) {
            edtMobile.setError("Field cannot be empty");
            return false;
        } else {
            edtMobile.setError(null);
            return true;
        }
    }
    private Boolean validatePassword() {
        String val = edtPassword.getText().toString();
        String passwordVal = "^" +
                //"(?=.*[0-9])" +         //at least 1 digit
                //"(?=.*[a-z])" +         //at least 1 lower case letter
                //"(?=.*[A-Z])" +         //at least 1 upper case letter
                "(?=.*[a-zA-Z])" +      //any letter
                "(?=.*[@#$%^&+=])" +    //at least 1 special character
                "(?=\\S+$)" +           //no white spaces
                ".{4,}" +               //at least 4 characters
                "$";

        if (val.isEmpty()) {
            edtPassword.setError("Field cannot be empty");
            return false;
        } else if (!val.matches(passwordVal)) {
            edtPassword.setError("Password is too weak");
            return false;
        } else {
            edtPassword.setError(null);

            return true;
        }
    }

}