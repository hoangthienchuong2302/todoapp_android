package com.example.assignment.Dialog;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.assignment.Event.DialogEvent;
import com.example.assignment.Modal.Todo;
import com.example.assignment.Modal.TodoType;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.example.assignment.Activity.MainActivity.user;
import static com.example.assignment.Activity.SplashActivity.idPublic;
import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;

public class AddTodoBottomSheet extends Dialog {

    public TextView tvStartDate, tvEndDate,tvNewItem,tvError;
    public EditText edtTitle, edtDesc;
    public ImageView imgCell,imgTick;
    public Button btnAddTodo;
    public Spinner spTodoType;
    Context context;
    public List<Todo> mData;
    public String isDone ="false";
    public int idTodoType;
    public List<TodoType> ListTodoType=new ArrayList<>();

    public ArrayList<String> list_Name = new ArrayList<>();
    private RetrofitSevice sevice;
    DialogEvent.AddTodo listener;
    Todo todo = null;
    public  String id;


    public AddTodoBottomSheet(Activity context, DialogEvent.AddTodo listener) {
        super(context);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.listener = listener;
    }

    public AddTodoBottomSheet(Activity context, Todo todo, DialogEvent.AddTodo listener) {
        super(context);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.listener = listener;
        this.todo = todo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_todo);

        tvStartDate = findViewById(R.id.tvStartDate);
        tvEndDate = findViewById(R.id.tvEndDate);
        edtTitle = findViewById(R.id.edtTitle);
        edtDesc = findViewById(R.id.edtDesc);
        spTodoType = findViewById(R.id.spTodoType);
        btnAddTodo = findViewById(R.id.btnAddTodo);
        imgCell= findViewById(R.id.imgCell);
        imgTick= findViewById(R.id.imgTick);
        tvError= findViewById(R.id.tvError);
        tvNewItem= findViewById(R.id.tvNewItem);

        imgCell.setVisibility(android.view.View.GONE);
        imgTick.setVisibility(android.view.View.VISIBLE);

        imgCell.setOnClickListener(view -> {
            isDone="false";
            Log.e("image", "onFailure: " + isDone);
            imgCell.setVisibility(android.view.View.GONE);
            imgTick.setVisibility(android.view.View.VISIBLE);
        });

        imgTick.setOnClickListener(view -> {
            isDone="true";
            Log.e("image", "onFailure: " + isDone);
            imgTick.setVisibility(android.view.View.GONE);
            imgCell.setVisibility(android.view.View.VISIBLE);
        });

        sevice = RetrofitBuilder.createService(RetrofitSevice.class, BASE_URL);
        Map<String, String> idUser = new HashMap<>();
        idUser.put("idUser", idPublic);

        sevice.listTodoType(idUser).enqueue(getListTodoTypeCallback);

        if (todo != null) {
            tvStartDate.setText(todo.getStartDate());
            tvEndDate.setText(todo.getEndDate());
            edtTitle.setText(todo.getTitle());
            edtDesc.setText(todo.getDesciption());
            spTodoType.setSelection(getSpinnerPosition(ListTodoType,todo));
            if(todo.getIsDone().equals("true")){
                imgTick.setVisibility(android.view.View.GONE);
                imgCell.setVisibility(android.view.View.VISIBLE);
            }else{
                imgTick.setVisibility(android.view.View.VISIBLE);
                imgCell.setVisibility(android.view.View.GONE);
            }
            btnAddTodo.setText("Update");
            tvNewItem.setText("Update Todo");
            id= String.valueOf(todo.getId());
        }

        Date today = new Date();
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);

        final int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        final int month = calendar.get(Calendar.MONTH);
        final int year = calendar.get(Calendar.YEAR);
        tvStartDate.setOnClickListener(view -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (datePicker, i, i1, i2) -> {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
                calendar.set(i, i1, i2);
                tvStartDate.setText(simpleDateFormat.format(calendar.getTime()));

            }, year, month, dayOfWeek);
            datePickerDialog.show();
        });
        tvEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
                        calendar.set(i, i1, i2);
                        tvEndDate.setText(simpleDateFormat.format(calendar.getTime()));

                    }
                }, year, month, dayOfWeek);
                datePickerDialog.show();
            }
        });
        btnAddTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( validateTitle() && validateDate()) {
                    Map<String, String> fields = new HashMap<>();
                    fields.put("title", edtTitle.getText().toString());
                    fields.put("desciption", edtDesc.getText().toString());
                    fields.put("startDate", tvStartDate.getText().toString());
                    fields.put("endDate", tvEndDate.getText().toString());
                    fields.put("idUser", idPublic);
                    fields.put("idTodoType", String.valueOf(idTodoType));
                    fields.put("isDone", String.valueOf(isDone));
                    if (todo != null) {
                        fields.put("id", String.valueOf(id));
                    }
                    listener.onComplete(fields);
                    dismiss();
                }
            }
        });
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    Callback<ArrayList<TodoType>> getListTodoTypeCallback = new Callback<ArrayList<TodoType>>() {

        @Override
        public void onResponse(Call<ArrayList<TodoType>> call, Response<ArrayList<TodoType>> response) {
            if (response.isSuccessful()) {
                ListTodoType = response.body();
                for (int j = 0; j < ListTodoType.size(); j++) {
                    list_Name.add(String.valueOf(ListTodoType.get(j).getTypeName()));
                }
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, list_Name);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                spTodoType.setAdapter(arrayAdapter);
                spTodoType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        Log.e("TAG", "onItemSelected: " + adapterView.getItemAtPosition(i).toString());
                        for (int j = 0; j < ListTodoType.size(); j++) {
                            if (adapterView.getItemAtPosition(i).toString().equalsIgnoreCase(ListTodoType.get(j).getTypeName())) {
                                idTodoType = ListTodoType.get(j).getId();
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            } else {
                Log.e("b", "onResponse: " + response.message());
            }
        }

        @Override
        public void onFailure(Call<ArrayList<TodoType>> call, Throwable t) {
            Log.e("image", "onFailure: " + t.getMessage());
        }
    };

    private int getSpinnerPosition(List<TodoType> todoTypeList, Todo todo) {
        Log.e("b", "onResponse: " + todoTypeList.size());
        for (int i = 0; i < todoTypeList.size(); i++){
            if(todoTypeList.get(i).getId() == todo.getIdTodoType()){
                return i;
            }
        }
        return 0;
    }
    private Boolean validateTitle() {
        String val = edtTitle.getText().toString();

        if (val.isEmpty()) {
            edtTitle.setError("Field cannot be empty");
            return false;
        }
        else {
            edtTitle.setError(null);
            return true;
        }
    }
    private Boolean validateDate() {
        String start = tvStartDate.getText().toString();
        String end = tvEndDate.getText().toString();

        if (start.equals("Enter start date") | end.equals("Enter end date")) {
            tvError.setText("You have not selected a date");
            return false;
        }else if(start.compareTo(end)>0){
            tvError.setText("You chose the wrong date");
            return false;
        }
        else {
            tvError.setText(null);
            return true;
        }
    }
}