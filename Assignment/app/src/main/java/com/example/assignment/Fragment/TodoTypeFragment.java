package com.example.assignment.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.assignment.Activity.MainActivity;
import com.example.assignment.Adapter.TodoTypeAdapter;
import com.example.assignment.Dialog.AddTodoTypeBottomSheet;
import com.example.assignment.Modal.TodoType;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


import static com.example.assignment.Activity.MainActivity.idFinal;
import static com.example.assignment.Activity.MainActivity.user;
import static com.example.assignment.Activity.SplashActivity.idPublic;
import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;


public class TodoTypeFragment extends Fragment {
    public static RecyclerView rcvTodoType;
    public static TodoTypeAdapter todoTypeAdapter;
    public static List<TodoType> mData;
    public FloatingActionButton fabAddTodoType;
    public EditText searchInput;
    public CharSequence search="";
    public ImageView imgTodoType;
    public Map<String, String> idUser= new HashMap<>();

    private RetrofitSevice sevice;
    Context context;
    int RequestCodeImage = 123;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    String realPath = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_add_todo_type,container,false);
        verifyStoragePermissions(getContext());
        fabAddTodoType = view.findViewById(R.id.fabAddTodoType);
        searchInput = view.findViewById(R.id.search_input);
        rcvTodoType = view.findViewById(R.id.rcvTodoType);

        mData = new ArrayList<>();

        rcvTodoType.setLayoutManager(new LinearLayoutManager(context));
        setRecyclerView(mData);

        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);

        idUser.put("idUser", idPublic);
        mData.clear();
        sevice.listTodoType(idUser).enqueue(getListTodoTypeCallback);


        fabAddTodoType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTodoTypeBottomSheet addTodoTypeBottomSheet= new AddTodoTypeBottomSheet();
                addTodoTypeBottomSheet.show(((AppCompatActivity) getContext()).getSupportFragmentManager(),addTodoTypeBottomSheet.getTag());
            }
        });

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                todoTypeAdapter.getFilter().filter(s);
                search = s;
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    Callback<ArrayList<TodoType>> getListTodoTypeCallback = new Callback<ArrayList<TodoType>>(){

        @Override
        public void onResponse(Call<ArrayList<TodoType>> call, Response<ArrayList<TodoType>> response) {
            if(response.isSuccessful()){
                mData.clear();
                if(response.body() != null) {
                    mData = response.body();
                }
                setRecyclerView(mData);

            }else{
                Log.e("b", "onResponse: "+response.message());
            }
        }

        @Override
        public void onFailure(Call<ArrayList<TodoType>> call, Throwable t) {
            Log.e("image", "onFailure: " + t.getMessage());
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RequestCodeImage && resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();
            realPath = getRealPathFromUri(uri);
            try {
                InputStream inputStream = getContext().getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imgTodoType.setImageBitmap(bitmap);
                Log.e("TAG", "onActivityResult: "+uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public String getRealPathFromUri(Uri contentUri){
        String path = null;
        String[] project = { MediaStore.MediaColumns.DATA };
        Cursor cursor = getContext().getContentResolver().query(contentUri, project, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(columnIndex);
        }
        cursor.close();
        return path;
    }
    public static void verifyStoragePermissions(Context context) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    (Activity) context,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
    public static void setRecyclerView(List<TodoType> data){
        todoTypeAdapter= new TodoTypeAdapter(rcvTodoType.getContext(), data );
        todoTypeAdapter.notifyDataSetChanged();
        rcvTodoType.setAdapter(todoTypeAdapter);
    }

}
