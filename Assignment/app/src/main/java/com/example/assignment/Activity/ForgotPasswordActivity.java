package com.example.assignment.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.assignment.Modal.User;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;

import java.io.Console;
import java.util.HashMap;
import java.util.Map;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;

public class ForgotPasswordActivity extends AppCompatActivity {
    public EditText edtEmail;
    public Button btnDialog;
    public CircularProgressButton cirSendButton;
    private RetrofitSevice sevice;
    public Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        setContentView(R.layout.activity_forgot_password);
        edtEmail= findViewById(R.id.edtEmail);
        cirSendButton= findViewById(R.id.cirSendButton);
        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);

        cirSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> fields= new HashMap<>();
                fields.put("email", edtEmail.getText().toString());
                sevice.forgot(fields).enqueue(getForgotPasswordCallback);
            }
        });
    }
    Callback<User> getForgotPasswordCallback = new Callback<User>(){

        @Override
        public void onResponse(Call<User> call, Response<User> response) {
            if(response.isSuccessful()){
                openDialog();
            }else{
                Log.e("b", "onFailure: " + response.message());
            }
        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };
    public void openDialog() {
        final Dialog dialog = new Dialog(ForgotPasswordActivity.this); // Context, this, etc.
        dialog.setContentView(R.layout.dialog_verify);
        btnDialog=dialog.findViewById(R.id.btnDialog);
        dialog.setTitle("Verification Email");
        btnDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ForgotPasswordActivity.this,LoginActivity.class));
                overridePendingTransition(R.anim.slide_in_left,android.R.anim.slide_out_right);
            }
        });
        dialog.show();
    }
    public void onLoginClick(View view){
        startActivity(new Intent(this,LoginActivity.class));
        overridePendingTransition(R.anim.slide_in_left,android.R.anim.slide_out_right);

    }
}