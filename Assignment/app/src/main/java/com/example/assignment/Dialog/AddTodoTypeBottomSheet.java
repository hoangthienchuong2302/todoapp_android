package com.example.assignment.Dialog;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.assignment.Adapter.TodoTypeAdapter;
import com.example.assignment.Modal.TodoType;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.example.assignment.Activity.MainActivity.user;
import static com.example.assignment.Activity.SplashActivity.idPublic;
import static com.example.assignment.Fragment.TodoTypeFragment.rcvTodoType;
import static com.example.assignment.Fragment.TodoTypeFragment.setRecyclerView;
import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;

public class AddTodoTypeBottomSheet extends BottomSheetDialogFragment {
    public TodoTypeAdapter todoTypeAdapter;
//    public List<TodoType> mData;
    public EditText  edtTypeName;
    public ImageView imgTodoType;
    public Button btnAddTodoType;
    public String id,img_URl="";
    Map<String, String> fields = new HashMap<>();

    private RetrofitSevice sevice;
    Context context;
    int RequestCodeImage = 123;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    String realPath = "";
    int touch=0;
    public AddTodoTypeBottomSheet(){
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view   = inflater.inflate( R.layout.bottom_sheet_add_todo_type,container,false );
        verifyStoragePermissions(getContext());
        edtTypeName=view.findViewById(R.id.edtTypeName);
        imgTodoType=view.findViewById(R.id.imgTodoType);
        btnAddTodoType=view.findViewById(R.id.btnAddTodoType);

        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);

        imgTodoType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,RequestCodeImage);
                touch++;
            }
        });

        btnAddTodoType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateTypeName()) {

                    fields.put("typeName", edtTypeName.getText().toString());
                    fields.put("idUser", idPublic);
                    if (touch == 0) {
                        fields.put("image", BASE_URL + "image/");
                    } else {
                        fields.put("image", BASE_URL + "image/" + img_URl);
                    }

                    sevice.addTodoType(fields).enqueue(getAddTodoTypeCallback);
                    dismiss();
                }
            }
        });

        return view;
}
    Callback<ArrayList<TodoType>> getListTodoTypeCallback = new Callback<ArrayList<TodoType>>(){

        @Override
        public void onResponse(Call<ArrayList<TodoType>> call, Response<ArrayList<TodoType>> response) {
            if(response.isSuccessful()){
                ArrayList<TodoType> mData = response.body();
                setRecyclerView(mData);

            }else{
                Log.e("b", "onResponse: "+response.message());
            }
        }

        @Override
        public void onFailure(Call<ArrayList<TodoType>> call, Throwable t) {
            Log.e("image", "onFailure: " + t.getMessage());
        }
    };
    Callback<String> getImageCallback = new Callback<String>(){

        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            if(response.isSuccessful()){
                img_URl=response.body();

            }else{
                Log.e("b", "onResponse: "+response.message());
            }
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            Log.e("image", "onFailure: " + t.getMessage());
        }
    };


    Callback<TodoType> getAddTodoTypeCallback = new Callback<TodoType>(){

        @Override
        public void onResponse(Call<TodoType> call, Response<TodoType> response) {
            if(response.isSuccessful()){
                sevice.listTodoType(fields).enqueue(getListTodoTypeCallback);
            }else{
                Log.e("b", "onResponse: "+response.message());
            }
        }

        @Override
        public void onFailure(Call<TodoType> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RequestCodeImage && resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();
            realPath = getRealPathFromUri(uri);
            try {
                InputStream inputStream = getContext().getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imgTodoType.setImageBitmap(bitmap);
                Log.e("TAG", "onActivityResult: "+uri);
                File file = new File(realPath);
                String filePath = file.getAbsolutePath();
                String[] arrayFileName = filePath.split("\\.");

                filePath = arrayFileName[0] + System.currentTimeMillis() + "." + arrayFileName[1];
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("uploaded_file",filePath,requestBody);
                sevice.UploadImage(part).enqueue(getImageCallback);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public String getRealPathFromUri(Uri contentUri){
        String path = null;
        String[] project = { MediaStore.MediaColumns.DATA };
        Cursor cursor = getContext().getContentResolver().query(contentUri, project, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(columnIndex);
        }
        cursor.close();
        return path;
    }
    public static void verifyStoragePermissions(Context context) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    (Activity) context,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    private Boolean validateTypeName() {
        String val = edtTypeName.getText().toString();

        if (val.isEmpty()) {
            edtTypeName.setError("Field cannot be empty");
            return false;
        }
        else {
            edtTypeName.setError(null);
            return true;
        }
    }
}
