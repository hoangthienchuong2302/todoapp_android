package com.example.assignment.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.assignment.Adapter.TodoAdapter;
import com.example.assignment.Dialog.AddTodoBottomSheet;
import com.example.assignment.Event.TodoEvent;
import com.example.assignment.Modal.Todo;
import com.example.assignment.Modal.TodoType;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.assignment.Activity.MainActivity.user;
import static com.example.assignment.Activity.SplashActivity.idPublic;
import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;


public class TodoListFragments extends Fragment implements TodoEvent.OnItemClick {
    public static RecyclerView rcvTodo;
    public static List<Todo> todoList;
    public List<TodoType> ListTodoType= new ArrayList<>();
    public ArrayList<String> list_Name = new ArrayList<>();
    public FloatingActionButton fabAddTodo;
    public Map<String, String> idUser= new HashMap<>();
    public Spinner spTodoType;

    public int idTodoType;
    private RetrofitSevice sevice;
    Context context;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo, container, false);
        fabAddTodo = view.findViewById(R.id.fabAddTodo);
        rcvTodo = view.findViewById(R.id.rcvTodo);
        spTodoType = view.findViewById(R.id.spTodoType);

        todoList = new ArrayList<>();

        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);



        idUser.put("idUser", idPublic);

        sevice.listTodoType(idUser).enqueue(getListTodoTypeCallback);

        todoList.clear();
        fabAddTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTodoBottomSheet addTodoBottomSheet = new AddTodoBottomSheet(getActivity(), body -> {
                    sevice.addTodo(body).enqueue(getAddTodoCallback);
                });
                addTodoBottomSheet.show();
                addTodoBottomSheet.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                addTodoBottomSheet.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });

        TodoAdapter todoAdapter = new TodoAdapter(context, todoList, this);
        todoAdapter.notifyDataSetChanged();
        loadTodo(idUser);
        return view;
    }


    private void loadTodo(Map<String, String> ID){
        Call<ArrayList<Todo>> callback = sevice.listTodo(idUser);
        callback.enqueue(new Callback<ArrayList<Todo>>() {
            @Override
            public void onResponse(Call<ArrayList<Todo>> call, Response<ArrayList<Todo>> response) {
                if(response.isSuccessful()){
                    todoList.clear();
                    todoList = (List<Todo>) response.body();
                    setRecyclerView(todoList);
                }else{
                    Log.e("b", "onResponse: "+response.message());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Todo>> call, Throwable t) {

            }
        });
    }

    Callback<Todo> getAddTodoCallback = new Callback<Todo>(){

        @Override
        public void onResponse(Call<Todo> call, Response<Todo> response) {
            if(response.isSuccessful()){
                loadTodo(idUser);
            }else{
                Log.e("b", "onResponse: "+response.message());
            }
        }

        @Override
        public void onFailure(Call<Todo> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };

    private void setRecyclerView(List<Todo> todoList){
        TodoAdapter todoAdapter = new TodoAdapter(context, todoList, this);
        rcvTodo.setHasFixedSize(true);
        rcvTodo.setAdapter(todoAdapter);
        rcvTodo.setLayoutManager(new LinearLayoutManager(context));
        todoAdapter.notifyDataSetChanged();
        Log.e("SizeTodoList", "onResponse: "+todoList.size() );
    }

    @Override
    public void GetItemPosition(int position) {

        AddTodoBottomSheet addTodoBottomSheet = new AddTodoBottomSheet(getActivity() ,todoList.get(position), body -> {
            Log.e("SizeTodoList", "onResponse: "+body );
            sevice.updateTodo(body).enqueue(getAddTodoCallback);
        });
        addTodoBottomSheet.show();
        addTodoBottomSheet.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        addTodoBottomSheet.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
    Callback<ArrayList<TodoType>> getListTodoTypeCallback = new Callback<ArrayList<TodoType>>() {

        @Override
        public void onResponse(Call<ArrayList<TodoType>> call, Response<ArrayList<TodoType>> response) {
            if (response.isSuccessful()) {
                ListTodoType = response.body();
                for (int j = 0; j < ListTodoType.size(); j++) {
                    list_Name.add(String.valueOf(ListTodoType.get(j).getTypeName()));
                }
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, list_Name);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                spTodoType.setAdapter(arrayAdapter);
                spTodoType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        for (int j = 0; j < ListTodoType.size(); j++) {
                            if (adapterView.getItemAtPosition(i).toString().equalsIgnoreCase(ListTodoType.get(j).getTypeName())) {
                                idTodoType = ListTodoType.get(j).getId();
                                Map<String, String> fields = new HashMap<>();
                                fields.put("idUser", idPublic);
                                fields.put("idTodoType", String.valueOf(idTodoType));
                                sevice.listTodoByIdTodoType(fields).enqueue(getListTodoCallback);
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            } else {
                Log.e("b", "onResponse: " + response.message());
            }
        }

        @Override
        public void onFailure(Call<ArrayList<TodoType>> call, Throwable t) {
            Log.e("image", "onFailure: " + t.getMessage());
        }
    };
    Callback<ArrayList<Todo>> getListTodoCallback = new Callback<ArrayList<Todo>>(){

        @Override
        public void onResponse(Call<ArrayList<Todo>> call, Response<ArrayList<Todo>> response) {
            if(response.isSuccessful()){
                todoList=response.body();
                TodoAdapter todoAdapter = new TodoAdapter(context, todoList, TodoListFragments.this);
                rcvTodo.setHasFixedSize(true);
                rcvTodo.setAdapter(todoAdapter);
                rcvTodo.setLayoutManager(new LinearLayoutManager(context));
                todoAdapter.notifyDataSetChanged();
            }else{
                Log.e("b", "onResponse: "+response.message());
            }
        }

        @Override
        public void onFailure(Call<ArrayList<Todo>> call, Throwable t) {
            Log.e("image", "onFailure: " + t.getMessage());
        }
    };

}
