package com.example.assignment.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.assignment.Modal.User;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;

import java.util.HashMap;
import java.util.Map;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    public EditText edtEmail,edtPassword,edtConfirmPassword;
    public CircularProgressButton cirhChangeButton;
    private RetrofitSevice sevice;
    TextView tvForgot;
    private static String BASE_URL="http://192.168.1.99:8070/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        edtEmail= findViewById(R.id.edtEmail);
        edtPassword= findViewById(R.id.edtPassword);
        edtConfirmPassword= findViewById(R.id.edtConfirmPassword);
        cirhChangeButton= findViewById(R.id.cirhChangeButton);
        tvForgot= findViewById(R.id.tvForgot);
        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);

        tvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChangePasswordActivity.this, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
            }
        });

        cirhChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> fields= new HashMap<>();
                fields.put("email", edtEmail.getText().toString());
                fields.put("password", edtPassword.getText().toString());
                fields.put("confirm_password", edtConfirmPassword.getText().toString());
                sevice.changePassword(fields).enqueue(getChangePasswordCallback);
            }
        });
    }

    Callback<User> getChangePasswordCallback = new Callback<User>(){

        @Override
        public void onResponse(Call<User> call, Response<User> response) {
            if(response.isSuccessful()){
                startActivity(new Intent(ChangePasswordActivity.this,LoginActivity.class));
                overridePendingTransition(R.anim.slide_in_left,android.R.anim.slide_out_right);
            }else{
                Toast.makeText(ChangePasswordActivity.this, "Login Failed!", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };

    public void onLoginClick(View view){
        startActivity(new Intent(this,LoginActivity.class));
        overridePendingTransition(R.anim.slide_in_left,android.R.anim.slide_out_right);

    }
}