package com.example.assignment.MyRetrofit;



import com.example.assignment.Modal.Todo;
import com.example.assignment.Modal.TodoType;
import com.example.assignment.Modal.User;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;

import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface RetrofitSevice {

    @POST("views/user_register.php")
    Call<User> register(
            @Body Map<String, String> body
    );
    @POST("views/user_login.php")
    Call<User> login(
            @Body Map<String, String> body
    );

    @POST("views/user_login_byId.php")
    Call<User> loginById(
            @Body Map<String, String> body
    );

    @POST("views/user_logOut.php")
    Call<User> logOUt(
            @Body Map<String, String> body
    );

    @POST("views/user_forgot_password.php")
    Call<User> forgot(
            @Body Map<String, String> body
    );
    @POST("views/user_update_password.php")
    Call<User> changePassword(
            @Body Map<String, String> body
    );

    @POST("views/user_updateLogin.php")
    Call<User> updateLogin(
            @Body Map<String, String> body
    );

    @Multipart
    @POST("views/upload_image.php")
    Call<String> UploadImage(@Part MultipartBody.Part photo);

    @POST("views/todoType_add.php")
    Call<TodoType> addTodoType(
            @Body Map<String, String> body
    );

    @POST("views/todoType_delete.php")
    Call<TodoType> deleteTodoType(
            @Body Map<String, String> body
    );

    @POST("views/todoType_update.php")
    Call<TodoType> updateTodoType(
            @Body Map<String, String> body
    );

    @POST("views/todoType_show.php")
    Call<ArrayList<TodoType>> listTodoType(
            @Body Map<String, String> body
    );

    @POST("views/todo_add.php")
    Call<Todo> addTodo(
            @Body Map<String, String> body
    );
    @POST("views/todo_show.php")
    Call<ArrayList<Todo>> listTodo(
            @Body Map<String, String> body
    );

    @POST("views/todo_show by_idtodotype.php")
    Call<ArrayList<Todo>> listTodoByIdTodoType(
            @Body Map<String, String> body
    );

    @POST("views/todo_update.php")
    Call<Todo> updateTodo(
            @Body Map<String, String> body
    );

    @POST("views/todo_delete.php")
    Call<TodoType> deleteTodo(
            @Body Map<String, String> body
    );

    @POST("views/count_todo.php")
    Call<String> countTodo(
            @Body Map<String, String> body
    );

    @POST("views/count_doing.php")
    Call<String> countDoing(
            @Body Map<String, String> body
    );

    @POST("views/count_fail.php")
    Call<String> countFail(
            @Body Map<String, String> body
    );
}
