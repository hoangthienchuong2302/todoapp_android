package com.example.assignment.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.assignment.Modal.User;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;

import java.util.HashMap;
import java.util.Map;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;

public class LoginActivity extends AppCompatActivity {
    CircularProgressButton cirLoginButton;
    public User user;
    private EditText edtEmail,edtPassword;
    private TextView tvForgot,tvError;
    private RetrofitSevice sevice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        edtEmail= findViewById(R.id.edtEmail);
        edtPassword= findViewById(R.id.edtPassword);
        tvForgot= findViewById(R.id.tvForgot);
        tvError= findViewById(R.id.tvError);
        cirLoginButton= findViewById(R.id.cirLoginButton);
        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);

        cirLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateEmail() | validatePass()){
                    Map<String, String> fields= new HashMap<>();
                    fields.put("email", edtEmail.getText().toString());
                    fields.put("password", edtPassword.getText().toString());
                    sevice.login(fields).enqueue(getLoginCallback);
                }

            }
        });
        tvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);

            }
        });
    }
    Callback<User> getLoginCallback = new Callback<User>(){

        @Override
        public void onResponse(Call<User> call, Response<User> response) {

            if(response.body()!=null){
                Toast.makeText(LoginActivity.this, "Login success!", Toast.LENGTH_SHORT).show();
                user = response.body();
                Intent i = new Intent(LoginActivity.this,MainActivity.class);
                i.putExtra("id",user.getId()) ;
                overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
                startActivity(i);
                SharedPreferences sharedPreferences = getSharedPreferences("checkLogin",MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("id",user.getId());
                editor.apply();
                Map<String, String> idUser= new HashMap<>();
                idUser.put("id", String.valueOf(user.getId()));
                sevice.updateLogin(idUser).enqueue(getUpdateLoginCallback);
            }else{
                Toast.makeText(LoginActivity.this, "Login Failed!", Toast.LENGTH_SHORT).show();
                tvError.setText("Your password or password is incorrect");
            }
        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };

    Callback<User> getUpdateLoginCallback = new Callback<User>(){

        @Override
        public void onResponse(Call<User> call, Response<User> response) {

            if(response.body()!=null){

            }else{

            }
        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };

    public void onMainClick() {
        Intent i = new Intent(LoginActivity.this,MainActivity.class);
        i.putExtra("id",user.getId()) ;
        overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
    }

    public void onLoginClick(View View) {
        startActivity(new Intent(this, RegisterActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
    }

    public void onChangeClick(View View) {
        startActivity(new Intent(this, ChangePasswordActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
    }
    private Boolean validateEmail() {
        String val = edtEmail.getText().toString();

        if (val.isEmpty()) {
            edtEmail.setError("Field cannot be empty");
            return false;
        }
        else {
            edtEmail.setError(null);
            return true;
        }
    }
    private Boolean validatePass() {
        String val = edtPassword.getText().toString();

        if (val.isEmpty()) {
            edtPassword.setError("Field cannot be empty");
            return false;
        }
        else {
            edtPassword.setError(null);
            return true;
        }
    }
}