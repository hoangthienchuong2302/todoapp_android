package com.example.assignment.Adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.assignment.Dialog.AddTodoBottomSheet;
import com.example.assignment.Event.DialogEvent;
import com.example.assignment.Event.TodoEvent;
import com.example.assignment.Modal.Todo;
import com.example.assignment.Modal.TodoType;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.assignment.Activity.MainActivity.user;
import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoViewHolder> {
    Context context;
    List<Todo> mData ;
    List<Todo> mDataFiltered ;
    private RetrofitSevice sevice;
    TodoEvent.OnItemClick listener;
    public TodoAdapter(Context context, List<Todo> mData, TodoEvent.OnItemClick listener) {
        this.context = context;
        this.mData = mData;
        this.mDataFiltered = mData;
        this.listener = listener;
    }
    public class TodoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTitle,tvDescription,tvDate;
        ImageView imgDelete,imgEdit;
        View view;

        public TodoViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvDate = itemView.findViewById(R.id.tvDate);
            view = itemView.findViewById(R.id.view);
            imgDelete = itemView.findViewById(R.id.imgDelete);
            imgEdit = itemView.findViewById(R.id.imgEdit);


        }

        @Override
        public void onClick(View v) {
            Todo todo = mDataFiltered.get(getAdapterPosition());
        }
    }
    @NonNull
    @Override
    public TodoAdapter.TodoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.item_todo,viewGroup,false );
        TodoViewHolder todoViewHolder =new TodoViewHolder(view);
        return todoViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull TodoAdapter.TodoViewHolder todoViewHolder, int position) {
        todoViewHolder.tvTitle.setText(mDataFiltered.get(position).getTitle());
        todoViewHolder.tvDate.setText(mDataFiltered.get(position).getStartDate()+" - "+mDataFiltered.get(position).getEndDate());
        todoViewHolder.tvDescription.setText(mDataFiltered.get(position).getDesciption());

        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String now = simpleDateFormat.format(date);

        if(mDataFiltered.get(position).getStartDate().compareTo(now)>0 && mDataFiltered.get(position).getIsDone().equals("false")){
            todoViewHolder.view.setBackgroundResource(R.drawable.button_background_yellow);
        }
        else if(mDataFiltered.get(position).getStartDate().compareTo(now)<=0 && mDataFiltered.get(position).getEndDate().compareTo(now)>=0
        && mDataFiltered.get(position).getIsDone().equals("false")){
            todoViewHolder.view.setBackgroundResource(R.drawable.button_background_blue);
        }else if( mDataFiltered.get(position).getEndDate().compareTo(now)<0 && mDataFiltered.get(position).getIsDone().equals("false")){
            todoViewHolder.view.setBackgroundResource(R.drawable.button_background_red);
        }else{
            todoViewHolder.view.setBackgroundResource(R.drawable.button_background_green);
        }
        todoViewHolder.imgDelete.setOnClickListener((view)->{
            Map<String, String> fields= new HashMap<>();
            fields.put("id",String.valueOf(mDataFiltered.get(position).getId()));
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
            builder.setTitle("Delete");
            builder.setMessage("Do you delete "+mDataFiltered.get( position ).getTitle()+ "?");
            builder.setCancelable(true);
            builder.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
            sevice.deleteTodo(fields).enqueue(getDeleteTodoCallback);
            mDataFiltered.remove(position);
                            notifyDataSetChanged();                        }
                    });

            builder.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        });

        todoViewHolder.imgEdit.setOnClickListener((view)->{
            listener.GetItemPosition(position);
        });
    }


    Callback<TodoType> getDeleteTodoCallback = new Callback<TodoType>(){

        @Override
        public void onResponse(Call<TodoType> call, Response<TodoType> response) {
            if(response.isSuccessful()){
                notifyDataSetChanged();
            }else{
                Log.e("b", "onResponse: "+response.message());
            }
        }

        @Override
        public void onFailure(Call<TodoType> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };

    @Override
    public int getItemCount() {
        return mDataFiltered.size();
    }
}
