package com.example.assignment.Modal;

public class TodoType {


    private String typeName, image;
    private int idUser,id;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        idUser = idUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public TodoType(String typeName, String image, int idUser) {
        this.typeName = typeName;
        this.image = image;
        this.idUser = idUser;
    }
}
