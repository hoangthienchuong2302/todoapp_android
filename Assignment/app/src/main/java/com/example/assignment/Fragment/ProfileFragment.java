package com.example.assignment.Fragment;


import android.annotation.SuppressLint;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


import com.example.assignment.Activity.LoginActivity;
import com.example.assignment.Activity.MainActivity;
import com.example.assignment.Activity.RegisterActivity;
import com.example.assignment.Activity.SplashActivity;
import com.example.assignment.Dialog.AddTodoTypeBottomSheet;
import com.example.assignment.Modal.User;
import com.example.assignment.MyRetrofit.RetrofitBuilder;
import com.example.assignment.MyRetrofit.RetrofitSevice;
import com.example.assignment.R;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;


import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.example.assignment.Activity.MainActivity.user;
import static com.example.assignment.Activity.SplashActivity.idPublic;
import static com.example.assignment.MyRetrofit.RetrofitBuilder.BASE_URL;
import static com.example.assignment.R.drawable.iconuser;


public class ProfileFragment extends Fragment {
    public TextView tvName,tvEmail,tvPhoneNumber,tvTodo,tvDoing,tvFail;
    public CircleImageView imgAvarta;
    public CircularProgressButton cirLogoutButton;
    private RetrofitSevice sevice;
    @SuppressLint("ResourceType")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentprofile, container, false);
        tvName= view.findViewById(R.id.tvName);
        tvEmail= view.findViewById(R.id.tvEmail);
        tvPhoneNumber= view.findViewById(R.id.tvPhoneNumber);
        tvTodo= view.findViewById(R.id.tvTodo);
        tvFail= view.findViewById(R.id.tvFail);
        tvDoing= view.findViewById(R.id.tvDoing);
        cirLogoutButton= view.findViewById(R.id.cirLogoutButton);
        imgAvarta= view.findViewById(R.id.imgAvarta);
        sevice = RetrofitBuilder.createService(RetrofitSevice.class,BASE_URL);

        Map<String, String> idUser= new HashMap<>();
        idUser.put("id", idPublic);
        sevice.loginById(idUser).enqueue(new Callback<User>(){

            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if(response.body()!=null){
                    user = response.body();
                    tvName.setText(user.getFullName());
                    tvEmail.setText(user.getEmail());
                    tvPhoneNumber.setText(user.getPhoneNumber());
                    if(user.getImage().equals(BASE_URL+"image/")){
                        imgAvarta.setImageResource(R.drawable.iconuser);
                        Log.e("TAG", "onCreateView: " );
                    }else {
                        Picasso.get().load(user.getImage()).into(imgAvarta);

                    }

                }else{

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("c", "onFailure: " + t.getMessage());
            }
        });

        cirLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sevice.logOUt(idUser).enqueue(new Callback<User>(){

                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        if(response.body()!=null){
                            startActivity(new Intent(getActivity(), LoginActivity.class));

                        }else{

                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Log.e("c", "onFailure: " + t.getMessage());
                    }
                });
            }
        });

        Map<String, String> idCount= new HashMap<>();
        idCount.put("idUser", idPublic);
        idCount.put("type", "count");
        sevice.countTodo(idCount).enqueue(getCountTodoCallback);
        sevice.countDoing(idCount).enqueue(getCountDoingCallback);
        sevice.countFail(idCount).enqueue(getCountFailCallback);
        return view;
    }
    Callback<String> getCountTodoCallback = new Callback<String>(){

        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            if(response.body()!=null){
                String countTodo = response.body();
                tvTodo.setText(countTodo);
            }else{
            }
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };
    Callback<String> getCountDoingCallback = new Callback<String>(){

        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            if(response.body()!=null){
                String countDoing = response.body();
                tvDoing.setText(countDoing);
            }else{
            }
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };

    Callback<String> getCountFailCallback = new Callback<String>(){

        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            if(response.body()!=null){
                String countFail = response.body();
                tvFail.setText(countFail);
            }else{
            }
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            Log.e("c", "onFailure: " + t.getMessage());
        }
    };
}
