<?php

    include_once '../services/todo.php';

    include_once '../libs/php-jwt-master/src/BeforeValidException.php';
    include_once '../libs/php-jwt-master/src/ExpiredException.php';
    include_once '../libs/php-jwt-master/src/SignatureInvalidException.php';
    include_once '../libs/php-jwt-master/src/JWT.php';

    include_once '../libs/PHPMailer-master/src/PHPMailer.php';
    include_once '../libs/PHPMailer-master/src/SMTP.php';
    include_once '../libs/PHPMailer-master/src/Exception.php';

    include_once '../configs/core.php';



    class TodoController {
        private $todo_service;

        public function __construct()
        {
            $this->todo_service = new TodoService();
        }
        
        public function addTodo($title, $desciption, $startDate, $endDate
                                    , $idUser, $idTodoType, $isDone){
         
            $status = $this->todo_service->addTodo($title, $desciption, $startDate, $endDate
                                    , $idUser, $idTodoType, $isDone);
            
            return $status;
        }

        public function listTodo($idUser){
           return $this->todo_service->getListTodo($idUser);
        }

        public function listTodoByIdTodoType($idUser, $idTodoType){
           return $this->todo_service->getListTodoByIdTodoType($idUser, $idTodoType);
        }

        public function countTodoByDate($idUser, $type){
           return $this->todo_service->countTodoByDate($idUser, $type);
        }

        public function countDoingByDate($idUser, $type){
           return $this->todo_service->countDoingByDate($idUser, $type);
        }

        public function countFailByDate($idUser, $type){
           return $this->todo_service->countFailByDate($idUser, $type);
        }

        public function deleteTodo($id, $idUser){

            $status = $this->todo_service->deleteTodo($id, $idUser);
        
            return $status;
        }

        public function updateTodo($id, $idUser, $title, $desciption, $startDate, $endDate, $idTodoType, $isDone){
         
            $status = $this->todo_service->updateTodo($id, $idUser, $title, $desciption, $startDate, $endDate, $idTodoType, $isDone);
            
            return $status;
        }
    }

?>