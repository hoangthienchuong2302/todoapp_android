<?php

    include_once '../services/todoType.php';

    include_once '../libs/php-jwt-master/src/BeforeValidException.php';
    include_once '../libs/php-jwt-master/src/ExpiredException.php';
    include_once '../libs/php-jwt-master/src/SignatureInvalidException.php';
    include_once '../libs/php-jwt-master/src/JWT.php';

    include_once '../libs/PHPMailer-master/src/PHPMailer.php';
    include_once '../libs/PHPMailer-master/src/SMTP.php';
    include_once '../libs/PHPMailer-master/src/Exception.php';

    include_once '../configs/core.php';



    class TodoTypeController {
        private $todoType_service;

        public function __construct()
        {
            $this->todoType_service = new TodoTypeService();
        }
        
        public function addTodoType($typeName, $image,$idUser){
         
            $status = $this->todoType_service->addTodoType($typeName, $image, $idUser);
            
            return $status;
        }

        public function listTodoType($idUser){
           return $this->todoType_service->getListTodoType($idUser);
        }

        public function deleteTodoType($id){

            $status = $this->todoType_service->deleteTodoType($id);
        
            return $status;
        }

        public function updateTodoType($typeName, $image, $id, $idUser){
         
            $status = $this->todoType_service->updateTodoType($typeName, $image, $id, $idUser);
            
            return $status;
        }
    }

?>