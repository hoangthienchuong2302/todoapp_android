<?php
    include_once '../configs/database_config.php';
    include_once '../models/todoType.php';


    class TodoTypeService {
        private $connection;

        public function __construct()
        {
            $this->connection = (new Database())->getConnection();
        }

        public function addTodoType($typeName,$image, $idUser)
        {

            try {
                $q = "INSERT INTO tblTodoType(typeName, image, idUser) VALUES(:typeName,:image,:idUser)";
                $stmt = $this->connection->prepare($q);

                $stmt->bindParam(":typeName", $typeName);
                $stmt->bindParam(":image", $image);
                $stmt->bindParam(":idUser", $idUser);
                

                $this->connection->beginTransaction();
                
                if ($stmt->execute()) {
                    $this->connection->commit();
                    return true;
                } else {
                    $this->connection->rollBack();
                    return false;
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
            return false;
        }

        public function getListTodoType($idUser){

            try {
                $q = "SELECT id, typeName,image  from tblTodoType where idUser=:idUser";
                $stmt = $this->connection->prepare($q);

                $stmt->bindParam(":idUser", $idUser);
                                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    $listTodoType = array();
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        extract($row);
                        $todoType= array(
                            "id"=>$id,
                            "typeName"=>$typeName,
                            "image"=>$image,
                      
                        );
                        array_push($listTodoType, $todoType);
                    };
    
                    return $listTodoType;
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            return null;
        }

        public function deleteTodoType($id)
        {
            try {
                $q = "DELETE FROM tbltodotype WHERE id=:id";
                $stmt = $this->connection->prepare($q);

     
                $stmt->bindParam(":id", $id);
                

                $this->connection->beginTransaction();
                
                if ($stmt->execute()) {
                    $this->connection->commit();
                    return true;
                } else {
                    $this->connection->rollBack();
                    return false;
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
            return false;
        }

        public function updateTodoType($typeName, $image, $id, $idUser)
        {

            try {
                $q = "UPDATE tblTodoType SET typeName=:typeName, image=:image WHERE id=:id and idUser=:idUser";
                $stmt = $this->connection->prepare($q);

                $stmt->bindParam(":typeName", $typeName);
                $stmt->bindParam(":image", $image);
                $stmt->bindParam(":id", $id);
                $stmt->bindParam(":idUser", $idUser);
                

                $this->connection->beginTransaction();
                
                if ($stmt->execute()) {
                    $this->connection->commit();
                    return true;
                } else {
                    $this->connection->rollBack();
                    return false;
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
            return false;
        }
    }
?>