<?php
    include_once '../configs/database_config.php';
    include_once '../models/todo.php';


    class TodoService {
        private $connection;

        public function __construct()
        {
            $this->connection = (new Database())->getConnection();
        }

        public function addTodo($title, $desciption, $startDate, $endDate
                                    , $idUser, $idTodoType, $isDone)
        {

            try {
                $q = "INSERT INTO tblTodo(title, desciption, startDate, endDate, idUser, idTodoType,isDone) 
                VALUES(:title, :desciption, :startDate, :endDate, :idUser, :idTodoType, :isDone)";
                $stmt = $this->connection->prepare($q);

                $stmt->bindParam(":title", $title);
                $stmt->bindParam(":desciption", $desciption);
                $stmt->bindParam(":startDate", $startDate);
                $stmt->bindParam(":endDate", $endDate);
                $stmt->bindParam(":idUser", $idUser);
                $stmt->bindParam(":idTodoType", $idTodoType);
                $stmt->bindParam(":isDone", $isDone);


                $this->connection->beginTransaction();
                
                if ($stmt->execute()) {
                    $this->connection->commit();
                    return true;
                } else {
                    $this->connection->rollBack();
                    return false;
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
            return false;
        }
        public function getListTodo($idUser){

            try {
                $q = "SELECT id, title, desciption, startDate, endDate, idTodoType , isDone
                from tblTodo where idUser=:idUser ";
                $stmt = $this->connection->prepare($q);

                $stmt->bindParam(":idUser", $idUser);

                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    $listTodo = array();
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        extract($row);
                        $todo= array(
                            "id"=>$id,
                            "title"=>$title,
                            "desciption"=>$desciption,
                            "startDate"=>$startDate,
                            "endDate"=>$endDate,
                            "idTodoType"=>$idTodoType,
                            "isDone"=>$isDone,
                        );
                        array_push($listTodo, $todo);
                    };
    
                    return $listTodo;
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            return null;
        }

         public function getListTodoByIdTodoType($idUser, $idTodoType){

            try {
                $q = "SELECT id, title, desciption, startDate, endDate , isDone
                from tblTodo where idUser=:idUser and idTodoType=:idTodoType";
                $stmt = $this->connection->prepare($q);

                $stmt->bindParam(":idUser", $idUser);
                $stmt->bindParam(":idTodoType", $idTodoType);

                $stmt->execute();

                    $listTodo = array();
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        extract($row);
                        $todo= array(
                            "id"=>$id,
                            "title"=>$title,
                            "desciption"=>$desciption,
                            "startDate"=>$startDate,
                            "endDate"=>$endDate,
                            "isDone"=>$isDone,
                        );
                        array_push($listTodo, $todo);
                    };
    
                    return $listTodo;
                
            } catch (Exception $e) {
                echo $e->getMessage();
            }

        }

        public function countTodoByDate($idUser, $type){

            try {
                $q = "SELECT id, title, desciption, startDate, endDate, idTodoType  
                from tblTodo where idUser=:idUser and startDate > CURDATE() ";
                $stmt = $this->connection->prepare($q);

                $stmt->bindParam(":idUser", $idUser);

                $stmt->execute();
                $listTodo = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $todo= array(
                        "id"=>$id,
                        "title"=>$title,
                        "desciption"=>$desciption,
                        "startDate"=>$startDate,
                        "endDate"=>$endDate,
                        "idTodoType"=>$idTodoType,
                    );
                    array_push($listTodo, $todo);
                };    
                if($type==="count"){
                return $stmt->rowCount() ;
                }
                    return $listTodo;

 
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            return null;
        }
        public function countDoingByDate($idUser, $type){

            try {
                $q = "SELECT id, title, desciption, startDate, endDate, idTodoType  
                from tblTodo where idUser=:idUser  and startDate <= CURDATE() and CURDATE() <= endDate";
                $stmt = $this->connection->prepare($q);

                $stmt->bindParam(":idUser", $idUser);

                $stmt->execute();
                $listTodo = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $todo= array(
                        "id"=>$id,
                        "title"=>$title,
                        "desciption"=>$desciption,
                        "startDate"=>$startDate,
                        "endDate"=>$endDate,
                        "idTodoType"=>$idTodoType,
                    );
                    array_push($listTodo, $todo);
                };    
                if($type==="count"){
                return $stmt->rowCount() ;
                }
                    return $listTodo;

 
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            return null;
        }

        public function countFailByDate($idUser, $type){

            try {
                $q = "SELECT id, title, desciption, startDate, endDate, idTodoType  
                from tblTodo where idUser=:idUser  and  CURDATE() > endDate";
                $stmt = $this->connection->prepare($q);

                $stmt->bindParam(":idUser", $idUser);

                $stmt->execute();
                $listTodo = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $todo= array(
                        "id"=>$id,
                        "title"=>$title,
                        "desciption"=>$desciption,
                        "startDate"=>$startDate,
                        "endDate"=>$endDate,
                        "idTodoType"=>$idTodoType,
                    );
                    array_push($listTodo, $todo);
                };    
                if($type==="count"){
                return $stmt->rowCount() ;
                }
                    return $listTodo;

 
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            return null;
        }

        public function deleteTodo($id, $idUser)
        {
            try {
                $q = "DELETE FROM tbltodo WHERE id=:id and idUser=:idUser";
                $stmt = $this->connection->prepare($q);

     
                $stmt->bindParam(":idUser", $idUser);
                $stmt->bindParam(":id", $id);
                

                $this->connection->beginTransaction();
                
                if ($stmt->execute()) {
                    $this->connection->commit();
                    return true;
                } else {
                    $this->connection->rollBack();
                    return false;
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
            return false;
        }
        public function updateTodo($id, $idUser, $title, $desciption, $startDate, $endDate, $idTodoType, $isDone)
        {

            try {
                $q = "UPDATE tblTodo SET title=:title, desciption=:desciption, startDate=:startDate, endDate=:endDate, idTodoType=:idTodoType, isDone=:isDone WHERE idUser=:idUser and id=:id";
                $stmt = $this->connection->prepare($q);

                $stmt->bindParam(":title", $title);
                $stmt->bindParam(":desciption", $desciption);
                $stmt->bindParam(":startDate", $startDate);
                $stmt->bindParam(":endDate", $endDate);
                $stmt->bindParam(":idUser", $idUser);
                $stmt->bindParam(":idTodoType", $idTodoType);
                $stmt->bindParam(":id", $id);
                $stmt->bindParam(":isDone", $isDone);
                

                $this->connection->beginTransaction();
                
                if ($stmt->execute()) {
                    $this->connection->commit();
                    return true;
                } else {
                    $this->connection->rollBack();
                    return false;
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
            return false;
        }
        
    }
?>