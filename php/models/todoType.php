<?php
class todoType{
    private $id;
    private $typeName;
    private $image;
    private $idUser;
    function __construct($id, $typeName, $image,$idUser){
        $this->id=$id;
        $this->typeName=$typeName;
        $this-> idUser= $idUser;
        $this-> image= $image;
    }
    public function getId()
        {
            return $this->id;
        }
    public function typeName()
        {
            return $this->typeName;
        }
    public function getIdUser()
        {
            return $this->idUser;
        }        
    public function getImage()
        {
            return $this->image;
        }
    }