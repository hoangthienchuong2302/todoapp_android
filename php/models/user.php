<?php
class User{
    private $id;
    private $email;
    private $hash_password;
    private $phoneNumber;
    private $fullName;
    private $image;
    private $checkLogin;
    function __construct($id, $email, $hash_password,$phoneNumber,$fullName, $image, $checkLogin){
        $this->id=$id;
        $this->email=$email;
        $this->hash_password=$hash_password;
        $this->phoneNumber=$phoneNumber;
        $this->fullName=$fullName;
        $this-> checkLogin= $checkLogin;
        $this-> image= $image;
    }
    public function getId()
        {
            return $this->id;
        }
    public function getEmail()
        {
            return $this->email;
        }
    public function getHashPassword()
        {
            return $this->hash_password;
        }
    public function getPhoneNumber()
        {
            return $this->phoneNumber;
        }
    public function getFullName()
        {
            return $this->fullName;
        }
    public function getImage()
        {
            return $this->image;
        }
    public function getCheckLogin()
        {
            return $this->checkLogin;
        }
    }