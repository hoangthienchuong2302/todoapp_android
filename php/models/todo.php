<?php
class todoType{
    private $id;
    private $title;
    private $desciption;
    private $startDate;
    private $endDate;
    private $idUser;
    private $idTodoType;
    private $isDone;
    
    function __construct($id, $title, $desciption, $startDate, $endDate, $idUser, $idTodoType, $isDone){
        $this->id=$id;
        $this->title=$title;
        $this-> desciption= $desciption;
        $this-> startDate= $startDate;
        $this->endDate=$endDate;
        $this-> idUser= $idUser;
        $this-> isDone= $isDone;
        $this-> idTodoType= $idTodoType;
    }
    public function getId()
        {
            return $this->id;
        }
    public function getTitle()
        {
            return $this->title;
        }
    public function getDesciption()
        {
            return $this->desciption;
        }        
    public function getStartDate()
        {
            return $this->startDate;
        }
    public function getEndDate()
        {
            return $this->endDate;
        }

    public function getIdUser()
        {
            return $this->idUser;
        }        
    public function getIdTodoType()
        {
            return $this->idTodoType;
        }
    public function getIsDone()
        {
            return $this->isDone;
        }
    }