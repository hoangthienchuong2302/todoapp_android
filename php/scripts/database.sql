CREATE DATABASE if not EXISTS todoApp;
use todoApp;

CREATE TABLE IF NOT EXISTS tblUsers(
    id int(11) not null AUTO_INCREMENT PRIMARY KEY,
    hash_password varchar(60) not null UNIQUE,
    email varchar(255) not nulL,
    phoneNumber varchar(11) not nulL,
    fullName varchar(255) not nulL,
    image varchar(255) not null DEFAULT ''
);

CREATE TABLE IF NOT EXISTS tblPasswordRsets(
    id int(11) not null AUTO_INCREMENT PRIMARY KEY,
    token varchar(60) not null UNIQUE ,
    email varchar(255) not nulL ,
    created datetime not null DEFAULT now(),
    availabe bit not null DEFAULT 1
);

create table if not exists tblTodoType(
    id int(11) not null AUTO_INCREMENT PRIMARY KEY,
    typeName varchar(255) not null ,
    image varchar(255) not null DEFAULT '',
    idUser int(11) not null,
    FOREIGN key (idUser) REFERENCES tblUsers(id)
);

create table if not exists tblTodo(
    id int(11) not null AUTO_INCREMENT PRIMARY KEY,
    title varchar(255) not null ,
    desciption varchar(255) not null ,
    startDate date not null ,
    endDate date not null ,
    idUser int(11) not null,
    FOREIGN key (idUser) REFERENCES tblUsers(id)
    idTodoType int(11) not null,
    FOREIGN key (idTodoType) REFERENCES tblTodoType(id)
);

INSERT into tblUsers(hash_password,email,phoneNumber,fullName) values('$2y$10$93rCJuWDAINtMSK/iK6FmOKEFes.Aja4J9X4qsyivd3VBS2O4ZpcK',
                                                'CHUONGHTPS11594@FPT.EDU.VN','0933961814','Chương');

INSERT into tblTodoType(typeName) values('Study');
                                           